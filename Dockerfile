FROM python:3.8-alpine AS build

RUN apk add --no-cache git python3 python3-dev py3-pip libffi libffi-dev musl-dev gcc
RUN pip3 install pipenv

RUN git clone https://github.com/eschava/psmqtt.git /psmqtt

WORKDIR /psmqtt

RUN python3 -m venv /psmqtt
ENV PATH="/psmqtt/bin:$PATH" VIRTUAL_ENV="/psmqtt"
RUN pip install -r requirements.txt


FROM python:3.8-alpine

COPY --from=build /psmqtt /psmqtt

ENV PATH="/psmqtt/bin:$PATH" VIRTUAL_ENV="/psmqtt"
ENV PSMQTT_ID=docker

ADD psmqtt.conf /psmqtt/psmqtt.conf

CMD [ "/psmqtt/bin/python", "/psmqtt/psmqtt.py" ]
